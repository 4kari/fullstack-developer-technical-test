@extends('layout.app')

@section('css')
  <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection
@section('breadcrumb')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Jabatan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="page">Pekerja</li>
    </ol>
</div>
@endsection
@section('content')
<div class="card sm mb-4">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary"></h6>
    </div>
    <div class="card-body">
      <form action="{{url('employee/'.$data->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="_method" value="PATCH">
        <div class="form-group">
          <label for="nama">Nama</label>
          <input type="text" class="form-control" id="nama" name="nama" value="{{$data->nama}}">
        </div>
        <div class="form-group">
          <label for="NIP">NIP</label>
          <input type="text" class="form-control" id="NIP" name="nip"  value="{{$data->nip}}">
        </div>
        <div class="form-group">
          <label for="Jabatan">Jabatan</label>
          <select class="form-control" id="Jabatan" name="jabatan">
            @foreach ($position as $p)
                @if($data->position_id==$p->id)
                    <option value="{{$p->id}}" selected>{{$p->name}}</option>
                @else
                    <option value="{{$p->id}}">{{$p->name}}</option>
                @endif
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="Departemen">Departemen</label>
          <input type="text" class="form-control" id="Departemen" name="departemen" value="{{$data->departemen}}">
        </div>
        <div class="form-group">
          <label for="Tanggal_Lahir">Tanggal Lahir</label>
          <input type="text" class="form-control" id="Tanggal_Lahir" name="tgl" value="{{$data->tanggal_lahir}}">
        </div>
        
        <div class="form-group">
          <label for="Alamat">Alamat</label>
          <input type="textarea" class="form-control" id="Alamat" name="alamat" value="{{$data->alamat}}">
        </div>
        <div class="form-group">
          <label for="Nohp">Nomor Telepon</label>
          <input type="number" class="form-control" id="Nohp" name="nohp" value="{{$data->nohp}}">
        </div>

        <div class="form-group">
          <label>Agama</label>
          <div class="custom-control custom-radio">
            <input type="radio" id="islam" name="agama" class="custom-control-input" value="islam" <?php if($data->agama=="islam"){echo "checked";}?>>
            <label class="custom-control-label" for="islam">Islam</label>
          </div>
          <div class="custom-control custom-radio">
            <input type="radio" id="Kristen" name="agama" class="custom-control-input" value="kristen" <?php if($data->agama=="kristen"){echo "checked";}?>>
            <label class="custom-control-label" for="Kristen">Kristen</label>
          </div>
          <div class="custom-control custom-radio">
            <input type="radio" id="Budha" name="agama" class="custom-control-input" value="budha" <?php if($data->agama=="budha"){echo "checked";}?>>
            <label class="custom-control-label" for="Budha">Budha</label>
          </div>
          <div class="custom-control custom-radio">
            <input type="radio" id="Hindu" name="agama" class="custom-control-input" value="hindu" <?php if($data->agama=="hindu"){echo "checked";}?>>
            <label class="custom-control-label" for="Hindu">Hindu</label>
          </div>
        </div>
        <div class="form-group">
          <label for="foto">File Foto KTP</label>
          <input type="file" class="form-control-file" id="foto" name="foto" placeholder="foto">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>

      </form>
    </div>
</div>

@endsection

@push('js')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
@endpush

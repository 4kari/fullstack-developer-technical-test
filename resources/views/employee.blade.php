@extends('layout.app')

@section('css')
  <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection
@section('breadcrumb')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Karyawan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="page">Karyawan</li>
    </ol>
</div>
@endsection
@section('content')
<div class="card sm mb-4">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary"></h6>
    </div>
    <div class="card-body">
        <a class="btn btn-info btn-sm text-white " href="{{'employee/create'}}"><i class="fa fa-fw fa-plus"></i>tambah</a>
        <!-- <a class="btn btn-info" href="">tambah</a> -->
        <div class="table-responsive p-3">
            <table class="table align-items-center table-flush" id="dataTable">
              <thead class="thead-light">
                <tr>
                  <th>No.</th>
                  <th>Name</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                  @foreach($data as $d)
                    <tr>
                        <td>{{$d->id}}</td>
                        <td>{{$d->nama}}</td>
                        <td>{{$d->status}}</td>
                        <td>
                            <a href="{{url('employee/'.$d->id)}}" class="btn btn-warning btn-sm"><i class="fa fa-fw fa-eye"></i> lihat</a></br>
                            <a href="{{url('employee/'.$d->id.'/edit')}}" class="btn btn-info btn-sm"><i class="fa fa-fw fa-check"></i> Ubah</a>
                            <form action="{{url('employee/'.$d->id)}}" method="POST">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                                <button class="btn btn-danger btn-sm" type="submit"> <i class="fa fa-fw fa-trash"></i> hapus </button>
                            </form>
                        </td>
                    </tr>
                  @endforeach
              </tbody>
            </table>
          </div>
    </div>
</div>

@endsection

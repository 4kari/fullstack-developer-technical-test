@extends('layout.app')

@section('css')
  <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection
@section('breadcrumb')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Jabatan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="page">Jabatan</li>
    </ol>
</div>
@endsection
@section('content')

    <div class="container bootstrap snippets bootdey">
        <div class="panel-body inf-content">
            <div class="row">
                <div class="col-md-4">
                    <img alt="" style="width:600px;" title="" class="img-circle img-thumbnail isTooltip" src="https://bootdey.com/img/Content/avatar/avatar7.png" data-original-title="Usuario"> 
                </div>
                <div class="col-md-6">
                    <strong>Information</strong><br>
                    <div class="table-responsive">
                    <table class="table table-user-information">
                        <tbody>
                            <tr>        
                                <td>
                                    <strong>
                                        <span class="glyphicon glyphicon-asterisk text-primary"></span>
                                        nip                                                
                                    </strong>
                                </td>
                                <td class="text-primary">
                                    {{$data->nip}}     
                                </td>
                            </tr>
                            <tr>    
                                <td>
                                    <strong>
                                        <span class="glyphicon glyphicon-user  text-primary"></span>    
                                        Nama                                                
                                    </strong>
                                </td>
                                <td class="text-primary">
                                    {{$data->nama}}     
                                </td>
                            </tr>
                            <tr>        
                                <td>
                                    <strong>
                                        <span class="glyphicon glyphicon-cloud text-primary"></span>  
                                        Jabatan                                                
                                    </strong>
                                </td>
                                <td class="text-primary">
                                    {{$data->position_id}}     
                                </td>
                            </tr>
                            <tr>        
                                <td>
                                    <strong>
                                        <span class="glyphicon glyphicon-cloud text-primary"></span>  
                                        departemen                                                
                                    </strong>
                                </td>
                                <td class="text-primary">
                                    {{$data->departemen}}     
                                </td>
                            </tr>

                            <tr>        
                                <td>
                                    <strong>
                                        <span class="glyphicon glyphicon-bookmark text-primary"></span> 
                                        Tanggal Lahir                                                
                                    </strong>
                                </td>
                                <td class="text-primary">
                                    {{$data->tanggal_lahir}}     
                                </td>
                            </tr>
                            <tr>        
                                <td>
                                    <strong>
                                        <span class="glyphicon glyphicon-bookmark text-primary"></span> 
                                        Nomor Telepon                                               
                                    </strong>
                                </td>
                                <td class="text-primary">
                                    {{$data->nohp}}     
                                </td>
                            </tr>
                            <tr>        
                                <td>
                                    <strong>
                                        <span class="glyphicon glyphicon-bookmark text-primary"></span> 
                                        Agama                                              
                                    </strong>
                                </td>
                                <td class="text-primary">
                                    {{$data->agama}}     
                                </td>
                            </tr>
                            <tr>        
                                <td>
                                    <strong>
                                        <span class="glyphicon glyphicon-bookmark text-primary"></span> 
                                        Status                                               
                                    </strong>
                                </td>
                                <td class="text-primary">
                                    {{$data->status}}
                                </td>
                                <td>
                                    <form action="{{url('employee/'.$data->id)}}" method="post">
                                        @csrf
                                        <input type="hidden" name="_method" value="PATCH">
                                        @if($data->status=="aktif")
                                        <input type="hidden" name="status" value="tidak aktif">
                                        <input type="submit" class="btn btn-sm btn-danger" value="nonaktif">
                                        @else
                                        <input type="hidden" name="status" value="aktif">
                                        <input type="submit" class="btn btn-sm btn-success" value="aktif">
                                        @endif
                                    </form>

                                </td>
                            </tr>

                                                            
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>                                        

@endsection

@push('js')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script>
    $('#dataTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('position.index') }}",
        columns: [
            { data: 'DT_RowIndex', orderable: false, searchable: false, className: "text-center"},
            { data: 'name' }
        ]
    })
</script>
@endpush

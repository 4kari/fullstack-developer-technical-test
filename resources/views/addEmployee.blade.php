@extends('layout.app')

@section('css')
  <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection
@section('breadcrumb')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Jabatan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="page">Pekerja</li>
    </ol>
</div>
@endsection
@section('content')
<div class="card sm mb-4">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary"></h6>
    </div>
    <div class="card-body">
      <form action="{{url('employee')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label for="nama">Nama</label>
          <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan Nama">
        </div>
        <div class="form-group">
          <label for="NIP">NIP</label>
          <input type="text" class="form-control" id="NIP" name="nip"  placeholder="NIP">
        </div>
        <div class="form-group">
          <label for="Jabatan">Jabatan</label>
          <select class="form-control" id="Jabatan" name="jabatan">
            @foreach ($position as $p)
              <option value="{{$p->id}}">{{$p->name}}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="Departemen">Departemen</label>
          <input type="text" class="form-control" id="Departemen" name="departemen" placeholder="Departemen">
        </div>
        <div class="form-group">
          <label for="Tanggal_Lahir">Tanggal Lahir</label>
          <input type="text" class="form-control" id="Tanggal_Lahir" name="tgl" placeholder="Tanggal Lahir">
        </div>
        <div class="form-group">
          <label for="Tahun_Lahir">Tahun Lahir</label>
          <select class="form-control" id="Tahun_Lahir" name="tahun">
            @for ($i = 1990; $i < 2022; $i++)
              <option value="{{$i}}">{{$i}}</option>
            @endfor
          </select>
        </div>
        <div class="form-group">
          <label for="Alamat">Alamat</label>
          <textarea type="text" class="form-control" id="Alamat" name="alamat" placeholder="Alamat" rows="3"></textarea>
        </div>
        <div class="form-group">
          <label for="Nohp">Nomor Telepon</label>
          <input type="number" class="form-control" id="Nohp" name="nohp" placeholder="Nomor Telepon">
        </div>

        <div class="form-group">
          <label>Agama</label>
          <div class="custom-control custom-radio">
            <input type="radio" id="islam" name="agama" class="custom-control-input" value="islam">
            <label class="custom-control-label" for="islam">Islam</label>
          </div>
          <div class="custom-control custom-radio">
            <input type="radio" id="Kristen" name="agama" class="custom-control-input" value="kristen">
            <label class="custom-control-label" for="Kristen">Kristen</label>
          </div>
          <div class="custom-control custom-radio">
            <input type="radio" id="Budha" name="agama" class="custom-control-input" value="budha">
            <label class="custom-control-label" for="Budha">Budha</label>
          </div>
          <div class="custom-control custom-radio">
            <input type="radio" id="Hindu" name="agama" class="custom-control-input" value="hindu">
            <label class="custom-control-label" for="Hindu">Hindu</label>
          </div>
        </div>
        <div class="form-group">
          <label for="foto">File Foto KTP</label>
          <input type="file" class="form-control-file" id="foto" name="foto" placeholder="foto">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>

      </form>
    </div>
</div>

@endsection

@push('js')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="'js/huruf.js'"></script>
<script>
    $('#dataTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('position.index') }}",
        columns: [
            { data: 'DT_RowIndex', orderable: false, searchable: false, className: "text-center"},
            { data: 'name' }
        ]
    })
</script>
@endpush

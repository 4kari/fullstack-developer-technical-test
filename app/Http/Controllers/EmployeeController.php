<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Position;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Employee::all();
        return view('employee',['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Employee;
        $data = Position::all();
        return view('addEmployee',[
            'model'=>$model,
            'position'=>$data
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $model = new Employee;
        $data = Employee::where('nip', $request->nip)->get();
        if(count($data)==0){
            var_dump($data);
            //cek nip
            $model->nama = $request->nama;
            $model->nip = $request->nip;
            $model->position_id = $request->jabatan;
            $model->departemen = $request->departemen;
            $tanggal = $request->tgl." ".$request->tahun;
            $model->tanggal_lahir = $tanggal;
            $model->alamat = $request->alamat;
            $model->nohp = $request->nohp;
            $model->agama = $request->agama;
            $model->status = TRUE;
            $model->foto = $request->foto;
            $model->save();
            return redirect('employee');
            
        }else{
            // buat nambah pesan
            return redirect('employee');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Employee::find($id);
        return view('detailEmployee',['data'=>$data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Employee::find($id);
        $pos = Position::all();
        return view('editEmployee',[
            'data'=>$data,
            'position'=>$pos
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Employee::find($id);
        if(isset($request->status)){
            if($request->status="aktif"){
                $model->status = TRUE;
            }else{
                $model->status = FALSE;
            }
        }else{
            $model->nama = $request->nama;
            $model->nip = $request->nip;
            $model->position_id = $request->jabatan;
            $model->departemen = $request->departemen;
            $tanggal=$request->tgl." ".$request->tahun;
            $model->tanggal_lahir = $tanggal;
            $model->alamat = $request->alamat;
            $model->nohp = $request->nohp;
            $model->agama = $request->agama;
            $model->foto = $request->foto;
        }
        $model->save();
        return redirect('employee');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Employee::find($id);
        $model->delete();
        return redirect('employee');
    }
}
